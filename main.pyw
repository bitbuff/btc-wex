import threading
import sys
import urllib.request
import time
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QGridLayout, QWidget, QCheckBox, QSystemTrayIcon, \
    QSpacerItem, QSizePolicy, QMenu, QAction, QStyle, qApp
from PyQt5.QtCore import QSize
from PyQt5 import QtGui
from bs4 import BeautifulSoup
 
    
def get_html(url):
    conn = urllib.request.urlopen(url)
    html = conn.read().decode('utf-8')
    return html

class NotificationTray(QSystemTrayIcon):
    
    def notification(self):
        soup = BeautifulSoup(get_html('https://wex.nz'), 'html.parser')
        div = soup.find("div", {'class':'orderStats'})
        cost_currency = str(div.find("strong").text)
        rise_currency = str(div.findAll("strong")[1].text)
        info = cost_currency + "\n" + rise_currency 
        self.showMessage("Bitcoin", info)
        
    def notification_loop(self): 
        while 1:
            self.notification()
            time.sleep(60*20)

def main():
    app = QApplication([])
    tray_icon = NotificationTray(QtGui.QIcon("bitcoin.png"), app)
    menu = QMenu()
    showAction = menu.addAction("Show currency info")
    exitAction = menu.addAction("Exit")
    showAction.triggered.connect(tray_icon.notification)
    exitAction.triggered.connect(app.quit)
    tray_icon.setContextMenu(menu)
    
    tray_icon.show()
    threading.Thread(target=tray_icon.notification_loop, args=()).start()
        
    sys.exit(app.exec_())

    
if __name__ == '__main__':
    main()